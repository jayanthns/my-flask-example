import datetime

from flask import Blueprint, jsonify, request

from apps.users.authentication import login, logout
from apps.users.decorators import token_required
from apps.users.serializers import user_login_schema, user_schema, users_schema
from models.users import User

user_api_blueprint = Blueprint(
    'user_api', __name__,
)


@user_api_blueprint.route('/', methods=['GET', 'POST'])
def index():
    return jsonify({"success": True}), 200


@user_api_blueprint.route('/login', methods=['POST'])
def user_login():
    u = user_login_schema.load(request.get_json())
    if u.errors:
        return jsonify(u.errors), 401
    user = login(u.data.get('email'), u.data.get('password'))
    if not user:
        return jsonify({"message": "Invalid credentials"}), 400
    user.last_login = datetime.datetime.utcnow()
    user.save_to_db()
    auth_token = user.encode_auth_token(user)
    return jsonify({"message": "Successful login",
                    "auth_token": auth_token.decode("utf-8"),
                    "succcess": True}), 200


@user_api_blueprint.route('/logout', methods=['POST'])
def user_logout():
    auth_header = request.headers.get('Authorization')
    print(auth_header)
    if logout(auth_header):
        return jsonify({"message": "Successfully logged out",
                        "success": True}), 200
    return jsonify({"message": "Something went wrong. Please try again.",
                    "success": False}), 400


@user_api_blueprint.route('/new-user', methods=['POST'])
def register_new_user():
    u = user_schema.load(request.get_json())
    if u.errors:
        return jsonify({**u.errors, "success": False}), 401
    user_data = u.data
    user = User(
        email=user_data.get('email'),
        username=user_data.get('username')
    )
    user.password = user_data.get('password')
    user.save_to_db()
    return jsonify({"message": "New user registered successfully",
                    "success": True}), 201


@user_api_blueprint.route('/users', methods=['GET'])
@token_required
def all_users():
    users = User.query.all()
    result = users_schema.dump(users)
    return jsonify({"success": True, "users": result.data}), 200
