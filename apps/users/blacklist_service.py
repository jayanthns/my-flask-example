from application import db
from models.users import BlasklistToken


def save_token(token):
    blacklist_token = BlasklistToken(token=token)
    try:
        # insert the token
        db.session.add(blacklist_token)
        db.session.commit()
        return True
    except Exception as e:
        return False