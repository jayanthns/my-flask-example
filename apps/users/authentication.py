from apps.users.blacklist_service import save_token
from models.users import User


def login(email, password):

    user = User.query.filter_by(email=email).first()
    if not user or not user.check_password(password):
        return None
    return user


def logout(data):
    if data:
        auth_token = data.split(' ')[1]
    else:
        auth_token = ''

    if auth_token:
        resp = User.decode_auth_token(auth_token)
        if not isinstance(resp, str):
            # mark the token as blacklisted
            return save_token(token=auth_token)
        else:
            return False
    else:
        return False


def get_logged_in_user(new_request):
    # get the auth token
    auth_token = new_request.headers.get('Authorization')
    if not auth_token:
        return None
    auth_token = auth_token.split(' ')[1]
    resp = User.decode_auth_token(auth_token)
    print(resp)
    if isinstance(resp, str):
        return None  # Either token is exipred or invalid.
    return resp  # Token is valid
    # If token valid Ignoring Check user in database
    # try:
    #     return User.query.get(resp['id'])
    # except:
    #     return None
