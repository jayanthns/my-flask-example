from marshmallow import ValidationError, validate

from application import ma
from models.users import User


def validate_email(email):

    if User.query.filter_by(email=email).first():
        raise ValidationError("Email id is already exists.")


def validate_username(username):

    if User.query.filter_by(username=username).first():
        raise ValidationError("Username is already exists.")


class UserSchema(ma.Schema):
    created_on = ma.Function(lambda obj: obj.created_on.strftime("%d-%m-%y"))
    email = ma.Email(required=True, validate=validate_email)
    password = ma.String(
        required=True, validate=validate.Length(min=8, max=24))
    username = ma.String(
        required=True, validate=validate_username)
    # created_on = ma.Method("get_created_on")

    class Meta:
        # Fields to expose
        fields = ('id', 'email', 'password', 'username', 'created_on')
        load_only = ('password',)  # write-only fields
        dump_only = ('id', 'created_on')  # read-only fields

    # def get_created_on(self, obj):
        """Line 6 and this is combined"""
    #     return obj.created_on.strftime("%d-%m-%y")


user_schema = UserSchema()
users_schema = UserSchema(many=True)


class UserLoginSchema(ma.Schema):
    email = ma.Email(required=True)
    password = ma.String(
        required=True, validate=validate.Length(min=8, max=24))

    class Meta:
        # Fields to expose
        fields = ('email', 'password')
        load_only = ('password',)  # write-only fields


user_login_schema = UserLoginSchema()


# class UserModelSchema(ma.ModelSchema):
#     class Meta:
#         # Fields to expose
#         model = User


# user_schema = UserModelSchema()
# users_schema = UserModelSchema(many=True)
