from functools import wraps
from flask import request, jsonify

from apps.users.authentication import get_logged_in_user


def token_required(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        user = get_logged_in_user(request)
        if not user:
            return jsonify({"message": "Invalid token"}), 400
        return f(*args, **kwargs)

    return decorated
