from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_bcrypt import Bcrypt
from flask_marshmallow import Marshmallow

from config import config_by_name

db = SQLAlchemy()
flask_bcrypt = Bcrypt()
ma = Marshmallow()


def create_app(config_name, register_blueprint=True):
    app = Flask(__name__)
    app.config.from_object(config_by_name[config_name])
    db.init_app(app)
    flask_bcrypt.init_app(app)
    ma.init_app(app)

    if register_blueprint:
        from apps.index.views import index_blueprint
        app.register_blueprint(index_blueprint, url_prefix='/')

        from apps.users.api_v1.views import user_api_blueprint
        app.register_blueprint(user_api_blueprint, url_prefix='/api/user')

    return app
